#Views de Cuentas

from django.shortcuts import render, render_to_response,  get_object_or_404, redirect
from django.template import RequestContext
from django.http import HttpResponse, Http404, HttpResponseRedirect

from django.contrib.auth import (
    authenticate,
    get_user_model,
    login,
    logout
)

from .forms import UserLoginForm, UserRegisterForm


def login_view(request):
    mensaje = None
    """mensaje = ""
    if request.user.is_authenticated():
        return redirect('reserva:administrar')
    else:
        if request.method == "POST":
            form = loginForm(request.POST)
            username = request.POST['username']
            password = request.POST['password']
            user = authenticate(username=username, password=password)
            if user is not None and user.is_active:
                login(request, user)
                return redirect('reserva:administrar')
            else:
                mensaje = "Usuario y/o Password incorrectos"

        form = loginForm()
        ctx = {'form': form, 'mensaje': mensaje}
        return render(request, 'reserva/login.html', ctx)"""
    
    if request.user.is_authenticated:
        return redirect('index')
    else:
    
        if request.method == 'POST':
            form = UserLoginForm(request.POST)
            username = request.POST['usuario']
            password = request.POST['clave']
            user = authenticate(username=username, password=password)
            if user is not None and user.is_active:
                login(request, user)
                return HttpResponseRedirect('index')
            else:   
                
                mensaje = "Usuario y/o Password incorrectos"
                
        else:
            
            form = UserLoginForm()
            ctx = {'form': form, 'mensaje': "Datos Incorrectos"}
            return render(request, "login.html", ctx , {'mensaje':mensaje, 'form':form})
    ctx = {"form":form, "mensaje":"Datos incorrectos"}
    return  render(request, "login.html", ctx)


def logout_def(request):
    logout(request)
    return redirect('/')



def register_view(request):
    next = request.GET.get('next')
    form = UserRegisterForm(request.POST or None)
    if form.is_valid():
        user = form.save(commit=False)
        password = form.cleaned_data.get('password')
        user.set_password(password)
        user.save()
        new_user = authenticate(username=user.username, password=password)
        login(request, new_user)
        if next:
            return redirect(next)
        return redirect('/')

    context = {
        'form': form,
    }
    return render(request, "signup.html", context)


def logout_view(request):
    logout(request)
    return redirect('/')