from django import forms
from django.contrib.auth import (
    authenticate,
    get_user_model

)

User = get_user_model()



class UserLoginForm(forms.Form):
    usuario = forms.CharField()
    clave = forms.CharField(widget=forms.PasswordInput)



    def clean(self, *args, **kwargs):
        username = self.cleaned_data.get('usuario')
        password = self.cleaned_data.get('clave')

        if username:
            user = authenticate(username=username)
            if not user:
                raise forms.ValidationError('Usuario y/o Clave incorrectos')
        return super(UserLoginForm, self).clean(*args, **kwargs)

    def validate_password(self, *args, **kwargs):
        password = self.cleaned_data.get('clave')

        if password:
            password = authenticate(password=password)
            if not password:
                raise forms.ValidationError('Esta contraseña es incorrecta')
        return super(UserLoginForm, self).clean(*args, **kwargs)
    
      


class UserRegisterForm(forms.ModelForm):
    email = forms.EmailField(label='Email ')
    email2 = forms.EmailField(label='Confirmar Email')
    password = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = [
            'username',
            'email',
            'email2',
            'password'
        ]

    def clean(self, *args, **kwargs):
        email = self.cleaned_data.get('email')
        email2 = self.cleaned_data.get('email2')
        if email != email2:
            raise forms.ValidationError("Emails deben coincidir")
        email_qs = User.objects.filter(email=email)
        if email_qs.exists():
            raise forms.ValidationError(
                "Este email ya esta registrado")
        return super(UserRegisterForm, self).clean(*args, **kwargs)