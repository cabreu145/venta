"""cayaldjango URL Configuration []

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:

contiene una lista de los patrones utilizados por urlresolver.
conjunto de patrones que Django intentará comparar con la URL recibida para encontrar la vista correcta

Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from cayaldjango  import views



from cuentas.views import login_view, register_view, logout_view, logout_def


urlpatterns = [
    
    #path('accounts/login/', login_view),
    
    path('', login_view, name='home'),
    path('accounts/logout/', logout_def, name='salir'),
    path('accounts/register/', register_view),
    path('accounts/logout/', logout_view),
    path('index/', views.login_view, name='index'),
    path('index/productos/', views.product_view, name="product"),
    path('index/ordenes/', views.orden_view, name="ordenes"),
    path('agregar/', views.CreateProduct.as_view(), name="add_product"),
    path('modificar/<int:pk>/', views.EditProducto.as_view(), name="edit_product"),
    path('eliminar/<int:pk>/', views.DeleteProduct.as_view(), name="delete_product"),
    path('admin/', admin.site.urls),
    
    
] 
