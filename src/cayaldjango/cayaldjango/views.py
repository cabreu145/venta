from django.http import HttpResponse
from django.shortcuts import render # Es una práctica muy común cargar una plantilla, llenar un contexto y retornar un objeto 
from productos.models import Producto
from django.views.generic import (CreateView, UpdateView, DeleteView)
from productos.form import FormProduct 
from django.urls import reverse, reverse_lazy, resolve
from django.contrib.auth.decorators import login_required

@login_required #Sirve para decirle a la vista que se necesita validar el login
def home_view(request, *args, **kwargs):
    print(args, kwargs)
    print(request.user)
    #return HttpResponse("<h1> Hola mundo<h1/>")
    return render(request, "template.html", {})

@login_required
def product_view(request, *args, **kwargs ):
    all_prodcuts = Producto.objects.all()
    ctx = {
        'all_prodcuts': all_prodcuts
    }
    return render(request, "producto.html", ctx)
@login_required
def orden_view(request, *args, **kwargs):
    print(args, kwargs)
    print(request.user)
    return render(request, "ordenes.html", {})
    

 
class CreateProduct(CreateView):
    model = Producto
    template_name = "agregar.html"
    form_class = FormProduct
    success_url = reverse_lazy('product')


class EditProducto(UpdateView):
    model = Producto
    template_name = "modificar.html"
    form_class = FormProduct
    success_url = reverse_lazy('product')


class DeleteProduct(DeleteView):
    model = Producto
    template_name = "eliminar.html"
    success_url = reverse_lazy('product')

def login_view(request, *args, **kwargs ):
    return render(request, "template.html", {})