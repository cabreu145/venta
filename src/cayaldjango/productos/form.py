from django.forms import (ModelForm, TextInput)
from django  import forms
from django.utils.translation import ugettext, ugettext_lazy as _
from django.core.exceptions import ValidationError
import datetime
from .models import Producto

OPCIONES_DISTRIBUIDOR=(
    ('Pollos Hermanos',_('Pollos Hermanos')),
     ('Vegetales Don Marco',_('Vegetales Don Marco')),
      ('',_('')),
       ('',_('')),

)

class FormProduct(forms.ModelForm):
    class Meta:
        model = Producto
        fields = [ 'Producto','Distribuidor','Precio','Peso','Imagen','Descripcion']
        widgets = {
                   'Producto': forms.TextInput(attrs={'class':'form-control'}),
                   'Distribuidor': forms.Select(attrs={'class':'form-control'}, choices = OPCIONES_DISTRIBUIDOR),
                   'Precio': forms.NumberInput(attrs={'class':'form-control'}),
                   'Peso': forms.NumberInput(attrs={'class':'form-control'}),
                   #'Imagen': forms.ClearableFileInput(attrs={'type': 'camera'}),
                   'Descripcion' : TextInput( attrs={ 'class': 'form-control rounded-input rounded-input-med txt12',})
        }
        labels = {
            'Producto': _('Nombre del Producto'),
            'Distribuidor': _('Distribuidor'),
            'Precio': _('Valor monetario'),
            'Peso': _('Kilogramos'),
            'Imagen': _('Imagen del Producto'),
            'Descripcion': _('Fecha de entrada producto')
        }
        help_texts = {
            'Producto': _('Ingrese el nombre del producto'),
            'Distribuidor': _('Por favor escoja una opción'),
            'Precio': _('Solo se permiten 9 digitos enteros'),
            'Imagen': _('Seleccione la imagen correcta'),
            'Descripcion': _('No se permiten años previos al 2019'),
        }



class UploadForm(forms.Form):
 Imagen = forms.FileField(
        label='Selecciona un archivo'
    )

#Metodo encargado de validar si es nulo o negativo 

def clean_Precio(self):
    Precio = self.cleaned_data['Precio']
    if Precio <= 0:
        raise ValidationError(('El valor no puede ser negativo o nulo.'))
    return Precio

#Metodo para validar la fecha no sea mayor a la actual 
def clean_Descripcion(self):
    Descripcion = self.cleaned_data['Fecha de entrada producto']
    print(Descripcion)
    if Descripcion > datetime.date.today():
        raise ValidationError(('No se pueden ingresar Fechas futuras'))
    elif Descripcion.year < 2019:
        raise ValidationError(('No se pueden ingresar Fechas previas al 2019'))
    return Descripcion


        

            
            
            
            
        
            
       
       