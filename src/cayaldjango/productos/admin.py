from django.contrib import admin

# Register your models here.
from .models import Producto

admin.site.register(Producto)

''' Un modelo en Django es un tipo especial de objeto que se guarda en la base de datos'''
