from django.shortcuts import render, redirect, get_object_or_404
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponse
from .models import Producto
from .form import FormProduct
from files.forms import UploadForm

'''Una View es un lugar donde ponemos la "lógica" de nuestra aplicación'''

''' 
Pedirá información del Modelo
'''
#Create your views here 
def product_list_view(request):
    producto = Producto.object.all()
    form =  FormProduct(request.POST or None)
    if request.method == 'POST':
        if form.is_valid():
            form.save(commit = True)
            return redirect('product_list_view')
    #Manejo de paginacion
    page = request.GET.get('page',1)
    paginator = Paginator(productos,3)
    try:
        producto_page = paginator.page(page)
    except PageNotAnInteger:
        producto_page = paginator.page(1)
    except EmptyPage:
        producto_page = paginator.page(paginator.num_pages)
    
    return render(request, 'templates/producto.html', {'productos': producto_page, 'form': form})

def delete_producto(request, id):
    producto = get_object_or_404(Item, id=id)
    #producto = Producto.objects.filter(id=id)
    if  producto:
        producto.delete()
    return redirect ('product_list_view')

def edit_producto (request, id):
    producto = get_object_or_404(Item, id=id)
    form = ProductoCreationForm(request.POST or None, instance = producto)
    if request.method =='POST':
        if form.is_valid():
            form.save()
            return redirect(product_list_view)
    return render(request, 'templates/modificar.html', {'producto' : producto, 'form' : form} )




def post_new(request):
    form = PostForm()
    return render(request, 'productos/agregar.html', {'form': form}) 

def upload_file(request):
    if request.method == 'POST':
        form = UploadForm(request.POST, request.FILES)
        if form.is_valid():
        	newdoc = Document(filename = request.POST['filename'],Imagen = request.FILES['docfile'])
        	newdoc.save(form)
        	return redirect("uploads")
    else:
        form = UploadForm()
    #tambien se puede utilizar render_to_response
    #return render_to_response('upload.html', {'form': form}, context_instance = RequestContext(request))
    return render(request, 'upload.html', {'form': form})
