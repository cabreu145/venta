from django.db import models

''' definimos todos los objetos llamados Models '''
class Producto(models.Model):
    Producto     =  models.CharField(max_length=20,)
    Distribuidor =  models.TextField(max_length=100)
    Descripcion  =  models.DateField(auto_now_add=False, auto_now=False)
    Precio       =  models.DecimalField(max_digits=11, decimal_places=2)
    Peso         =  models.DecimalField(max_digits=5, decimal_places=2)
    Imagen       =  models.ImageField(upload_to='user_directory_path')

    def __str__(self):
        return self.Producto